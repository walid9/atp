'use strict';
$(document).ready(function() {
    $('#zero-configuration').DataTable( {
        "language": {
            "search": "بحث",
            "sLengthMenu": 'عرض <select name="zero-configuration_length" aria-controls="zero-configuration" class="custom-select custom-select-sm form-control form-control-sm">'+
                '<option value="10">10</option>'+
                '<option value="20">20</option>'+
                '<option value="30">30</option>'+
                '<option value="40">40</option>'+
                '<option value="50">50</option>'+
                '<option value="-1">All</option>'+
                '</select>',
            "emptyTable": "لا يوجد بيانات هنا"
        }
    } );
    // [ Zero-configuration ] start
    $('#zero-configuration').DataTable();

    // [ HTML5-Export ] start
    $('#key-act-button').DataTable({
        language: {
            emptyTable: "لا يوجد بيانات هنا"
        },
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });

    // [ Columns-Reorder ] start
    $('#col-reorder').DataTable({
        colReorder: true,
        language: {
            emptyTable: "لا يوجد بيانات هنا"
        },
    });

    // [ Fixed-Columns ] start
    $('#fixed-columns-left').DataTable({
        scrollY: "300px",
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        fixedColumns: true,
        language: {
            emptyTable: "لا يوجد بيانات هنا"
        },
    });
    $('#fixed-columns-left-right').DataTable({
        scrollY: "300px",
        scrollX: true,
        scrollCollapse: true,
        paging: false,
       // fixedColumns: true,
        fixedColumns: {
            leftColumns: 1,
            rightColumns: 1
        },
        language: {
            emptyTable: "لا يوجد بيانات هنا"
        },
    });
    $('#fixed-header').DataTable({
        fixedHeader: true,
        language: {
            emptyTable: "لا يوجد بيانات هنا"
        },
    });

    // [ Scrolling-table ] start
    $('#scrolling-table').DataTable({
        scrollY: 300,
        paging: false,
        keys: true,
        language: {
            emptyTable: "لا يوجد بيانات هنا"
        },
    });

    // [ Responsive-table ] start
    $('#responsive-table').DataTable({
        ordering: false,
        language: {
            emptyTable: "لا يوجد بيانات هنا"
        },
    });

    $('#responsive-table-model').DataTable({
        ordering: false,
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal({
                    header: function(row) {
                        var data = row.data();
                        return 'Details for ' + data[0] + ' ' + data[1];
                    }
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                    tableClass: 'table'
                })
            }
        },
        language: {
            emptyTable: "لا يوجد بيانات هنا"
        },
    });
});
