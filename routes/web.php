<?php


Route::get('/', function () {
    return view('welcome');
});

///////////////////////////////////////// Begin admin routes /////////////////////////////

Route::group(['middleware' => ['web'], 'as' => 'admin:', 'prefix' => 'admin-panel', 'namespace' => 'Admin'], function () {

    include 'admin/users.php';
});

///////////////////////////////////////// End Web routes /////////////////////////////
