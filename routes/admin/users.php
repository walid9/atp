<?php

// USERS
Route::resource('users', 'UsersController');
Route::get('active/{user}', 'UsersController@active')->name('users.active');
Route::get('unActive/{user}', 'UsersController@unActive')->name('users.unActive');
Route::post('get-new-address-form', 'UsersController@newAddressForm')->name('new.address.form');
Route::post('add-address/{user}', 'UsersController@addAddress')->name('add-address');
Route::post('ajax-edit/address', 'UsersController@ajaxUpdateAddress')->name('ajax.edit.address');
Route::post('delete/address', 'UsersController@ajaxDeleteAddress')->name('ajax.delete.address');
