<div class="m-header">
    <a class="mobile-menu" id="mobile-collapse1" href="#!"><span></span></a>
    <a href="" class="b-brand">
        <div>
            <img style="width: 40px;height: 40px;" src="{{asset('web/assets/img/CleanserFlatLogo.png')}}" class="rounded-circle">
        </div>
        <span class="b-title">انستا سيلز</span>
    </a>
</div>
<a class="mobile-menu" id="mobile-header" href="#!">
    <i class="feather icon-more-horizontal"></i>
</a>
<div class="collapse navbar-collapse">
    <ul class="navbar-nav ml-auto">
        <li>
            <div class="dropdown">

                <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="icon feather icon-bell"></i>
                    <span style=" background: #cccccc;
                    position: absolute;
                    top: 55px;
                    right: 98%;
                    background-color: #ff3c20;
                    border-radius: 0.8em;
                    -moz-border-radius: 0.8em;
                    -webkit-border-radius: 0.8em;
                    color: #ffffff;
                    display: inline-block;
                    font-weight: bold;
                    line-height: 1.2em;
                    margin-right: -42px;
                    margin-top: -38px;
                    text-align: center;
                    width: 1.2em;">0</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right notification">
                    <div class="noti-head">
                        <h6 class="d-inline-block m-b-0">الإشعارات</h6>
                        <div class="float-right">
                            <a href="" class="m-r-10">قراءه الجميع</a>
                        </div>
                    </div>
                    <ul class="noti-body">

                    </ul>
                    <div class="noti-footer">
                            <a href="">عرض الكل</a>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="dropdown drp-user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon feather icon-settings"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-notification">
                    <div class="pro-head">

                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
