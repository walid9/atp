@if($action == "show")
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">

                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5> جميع الاعضاء </h5>
                                        <h4 style="float: left;">
                                            <li class="fas fa-users"></li>
                                            Count : {{$report->count()}}
                                        </h4>
                                    </div>
                                    <div class="card-block table-border-style">
                                        <div class="table-responsive">
                                            @endif

                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>الاسم</th>
                                                    <th> الحساب الشخصى</th>
                                                    <th> رقم الموبيل</th>
                                                    <th>تاريخ التسجيل</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($report->count() != 0)
                                                    @foreach($report as  $row)
                                                        <tr>
                                                            <td>{{$row->id}}</td>
                                                            <td>{{$row->name}}</td>
                                                            <td>{{$row->email}}</td>
                                                            <td>{{$row->phone}}</td>
                                                            <td>{{$row->created_at->format('Y-m-d')}}</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <span> عفوا لا توجد بيانات .. </span>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>

                                            @if($action == "show")
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ form-element ] end -->
                </div>
                <!-- [ Main Content ] end -->
            </div>
        </div>
    </div>
@endif
