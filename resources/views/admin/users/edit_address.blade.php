<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>تعديل العنوان </h5>
        </div>
        <div class="card-body">

            <div class="container">
                @foreach($user->addresses as $key=>$address)

                    <form id="form-edit-address-{{$address->id}}" action="#" enctype="multipart/form-data">
                        <div class="row edit-address-{{$address->id}}">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">الاسم :</label>
                                    <input type="text" id="name{{$address->id}}" name="name" value="{{$address->name}}"
                                           class="form-control
                            {{ $errors->has('name') ? 'is-invalid' : '' }}">

                                    @if ($errors->has('name'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">العنوان :</label>
                                    <input type="text" id="address{{$address->id}}" name="address" value="{{$address->address}}"
                                           class="form-control
                            {{ $errors->has('address') ? 'is-invalid' : '' }}">

                                    @if ($errors->has('address'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">الهاتف :</label>
                                    <input type="text" id="phone{{$address->id}}" name="phone" value="{{$address->phone}}"
                                           class="form-control
                            {{ $errors->has('phone') ? 'is-invalid' : '' }}">

                                    @if ($errors->has('phone'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-2">
                                <label for="exampleInputEmail1">خط العرض (Latitude)</label>

                                <span class="asterisk" style="color: red;"> * </span>

                                <!-- Button trigger modal -->
                                <a href="#" data-toggle="modal"
                                   data-target=".bd-example-modal-lg">
                                    عرض الخريطة
                                </a>


                                <input type="text" name="lat" required id="lat{{$address->id}}"
                                       class="form-control form-control{{ $errors->has('lat') ? 'is-invalid' : '' }}"
                                       value="{{$address->lat}}">

                                @if ($errors->has('lat'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('lat') }}</strong>
                                    </div>
                                @endif
                            </div>


                            <div class="form-group col-2">
                                <label for="exampleInputEmail1">خط الطول (Longitude)</label>
                                <span class="asterisk" style="color: red;"> * </span>
                                <input type="text" name="long" required id="lng{{$address->id}}"
                                       class="form-control{{ $errors->has('long') ? 'is-invalid' : '' }}"
                                       value="{{$address->long}}">

                                @if ($errors->has('long'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('long') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-1">
                                <div class="form-group">
                                    <button class="btn btn-sm btn-outline-success"
                                            onclick="saveEditAddress({{$address->id}})"
                                            type="button"
                                            title="Save" style=" margin-top: 37px;"
                                    >
                                        حفظ                                    </button>
                                </div>
                            </div>


                                <div class="col-md-1">
                                    <div class="form-group">
                                        <button class="btn btn-sm btn-outline-danger"
                                                onclick="deleteDivAddress({{$address->id}})"
                                                title="Delete Address" type="button" style=" margin-top: 37px;">
                                            <li class="fas fa-trash"></li>
                                        </button>
                                    </div>
                                </div>



                            <div class="col-md-1 ajax-load-{{$address->id}}" style="display: none; margin-top: 38px;">
                                <img src="{{asset('images\vendor\vue-image-lightbox-carousel\src\ajax-loader.gif')}}"
                                     style="height: 27px;width: 26px;">
                            </div>
                        </div>
                    </form>

                    <!-- Modal -->
                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">اختر الموقع</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body" id="map">

                                </div>

                                <div class="modal-footer" style="direction: ltr;">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">غلق</button>
                                    {{--                <button type="button" class="btn btn-primary">Send message</button>--}}
                                </div>

                            </div>
                        </div>
                    </div>

                    <script defer
                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSvNhmIoBm2AeDEnk0QSTAutALNLu-I7Y&callback=initMap">
                    </script>

                    <script type="text/javascript">

                        var index = {!! json_encode($address->id) !!};

                        //Set up some of our variables.
                        var map; //Will contain map object.
                        var marker = false; ////Has the user plotted their location marker?

                        //Function called to initialize / create the map.
                        //This is called when the page has loaded.
                        function initMap() {

                            //The center location of our map.
                            var centerOfMap = new google.maps.LatLng(24.68731563631883, 46.719044971885445);


                            //Map options.
                            var options = {
                                center: centerOfMap, //Set center.
                                zoom: 7 //The zoom value.
                            };

                            //Create the map object.
                            map = new google.maps.Map(document.getElementById('map'), options);

                            //Listen for any clicks on the map.
                            google.maps.event.addListener(map, 'click', function (event) {
                                //Get the location that the user clicked.
                                var clickedLocation = event.latLng;
                                //If the marker hasn't been added.
                                if (marker === false) {
                                    //Create the marker.
                                    marker = new google.maps.Marker({
                                        position: clickedLocation,
                                        map: map,
                                        draggable: true //make it draggable
                                    });
                                    //Listen for drag events!
                                    google.maps.event.addListener(marker, 'dragend', function (event) {
                                        markerLocation();
                                    });
                                } else {
                                    //Marker has already been added, so just change its location.
                                    marker.setPosition(clickedLocation);
                                }
                                //Get the marker's location.
                                markerLocation();
                            });
                        }

                        //This function will get the marker's current location and then add the lat/long
                        //values to our textfields so that we can save the location.
                        function markerLocation() {
                            //Get location.
                            var currentLocation = marker.getPosition();
                            //Add lat and lng values to a field that we can save.
                            document.getElementById('lat'+index).value = currentLocation.lat(); //latitude
                            document.getElementById('lng'+index).value = currentLocation.lng(); //longitude
                        }


                        //Load the map when the page has finished loading.
                        google.maps.event.addDomListener(window, 'load', initMap);

                    </script>

                @endforeach

                <div>
                    <form method="post" action="{{route('admin:add-address',$user->id)}}" id="form-promotion"
                          enctype="multipart/form-data" class="product-prices-form">
                        @csrf

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    function deleteDivAddress(index) {

        $.ajax({
            url: "{{route('admin:ajax.delete.address')}}",
            type: "post",
            data: {index: index},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function () {
                $('.ajax-load-' + index).show();

            },
            success: function (data) {
                $('.ajax-load-' + index).hide();
                toastr.success(data['success']);
                $(".edit-address-" + index).remove();
            },
            error: function (jqXhr, json, errorThrown) {
                var errors = jqXhr.responseJSON;
                var errorsHtml = '';
                $.each(errors, function (key, value) {
                    errorsHtml += '<li>' + value + '</li>';
                });
                $('.ajax-load-' + index).hide();
                toastr.error(errorsHtml);
            }
        });
    }


    function saveEditAddress(index) {

        var form_data = new FormData();

        var name = $("#name"+index).val();
        var address = $("#address"+index).val();
        var phone = $("#phone"+index).val();
        var lat = $("#lat"+index).val();
        var long = $("#long"+index).val();
        form_data.append("name", name);
        form_data.append("address", address);
        form_data.append("phone", phone);
        form_data.append("lat", lat);
        form_data.append("long", long);
        form_data.append("index", index);

        // console.log(form_data);

        $.ajax({
            url: "{{route('admin:ajax.edit.address')}}",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function () {
                $('.ajax-load-' + index).show();
            },
            success: function (data) {
                $('.ajax-load-' + index).hide();
                toastr.success(data['success']);
            },
            error: function (jqXhr, json, errorThrown) {
                var errors = jqXhr.responseJSON;
                var errorsHtml = '';
                $.each(errors, function (key, value) {
                    errorsHtml += '<li>' + value + '</li>';
                });
                $('.ajax-load-' + index).hide();
                toastr.error(errorsHtml);
            }
        });
    }

    function addAddress() {

        var div_count = $("#div-count").val();

        $.ajax({
            url: "{{route('admin:new.address.form')}}",
            type: "post",
            data:{div_count:div_count},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $("#div-count").val(parseInt(parseInt(div_count) + parseInt(1)));
                $(".address-form").append(data);
                $('#name_'+$("#div-count").val()).show();
                $('#phone_'+$("#div-count").val()).show();
                $('#address_'+$("#div-count").val()).show();
                $('#lat_'+$("#div-count").val()).show();
                $('#long_'+$("#div-count").val()).show();
            }
        });
    }


</script>
