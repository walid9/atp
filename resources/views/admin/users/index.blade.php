@extends('admin/layout')

@section('title')
    العملاء
@endsection

@section('styles')
{{--    <link rel="stylesheet" href="{{asset('backend/assets/plugins/data-tables/css/datatables.min.css')}}">--}}
@endsection

@section('content')
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">@yield('title')</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href=""><i
                                                    class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">الجدول</a></li>
                                    <li class="breadcrumb-item"><a href="#!"></a>@yield('title')</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">


                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>البحث</h5>
                                        <div class="card-header-right">
                                            <div class="btn-group card-option">
                                                <button type="button" class="btn dropdown-toggle btn-icon"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                    <i class="feather icon-more-horizontal"></i>
                                                </button>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-block">

                                        <form action="{{route('admin:users.index')}}" method="get">
                                            <div class="row">

                                                <div class=" col-md-2 col-sm-6">
                                                    <label for="exampleInputEmail1">البحث</label>
                                                    <input type="text" name="search" class="form-control ">
                                                </div>

                                                <div class=" col-md-2 col-sm-6">
                                                    <label for="exampleInputEmail1">الترتيب ب</label>

                                                    <select name="sort" class="form-control">
                                                        <option value="">اختر</option>
                                                        <option value="first_name">الاسم</option>
                                                        <option value="email">الاميل</option>
                                                        <option value="phone">رقم الهاتف</option>
                                                        <option value="created_at">التاريخ</option>
                                                    </select>
                                                </div>

                                                <div class=" col-md-2 col-sm-6">
                                                    <label for="exampleInputEmail1">نوع الترتيب</label>

                                                    <select name="sort_type" class="form-control">
                                                        <option value="ASC">تصاعديا</option>
                                                        <option value="DESC">تنازلى</option>
                                                    </select>
                                                </div>

                                                <div class=" col-md-2 col-sm-6">
                                                    <label for="exampleInputEmail1">عرض</label>

                                                    <select name="show" class="form-control">
                                                        <option value="">اختر</option>
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </div>

                                                <div class=" col-md-2 col-sm-6">
                                                    <label for="exampleInputEmail1">من</label>
                                                    <input type="date" name="date_from" class="form-control ">
                                                </div>

                                                <div class=" col-md-2 col-sm-6">
                                                    <label for="exampleInputEmail1">الى</label>
                                                    <input type="date" name="date_to" class="form-control ">
                                                </div>

                                                <div class=" col-md-3 col-sm-6" style="padding-top: 29px;">
                                                    <button type="submit" class="btn btn-sm btn-primary">بحث</button>

                                                    <a href="{{ route('admin:users.index') }}">
                                                        <button type="button" class="btn btn-sm btn-danger">الغاء
                                                        </button>
                                                    </a>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                            <!-- [ HTML5 Export button ] start -->
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>عرض</h5>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-9" style="justify-content: space-between">
                                        <a href="{{ route('admin:users.create') }}"
                                           class="btn btn-rounded btn-success add-button"><i
                                                    class="feather icon-plus-circle"></i>
                                            إضافه
                                        </a>
                                    </div>

                                    </div>
                                    <div class="card-block">

                                        <div class="table-responsive">
                                            <table id="" class="display table nowrap table-hover" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>الصوره</th>
                                                    <th>الإسم</th>
                                                    <th>البريد الإلكترونى</th>
                                                    <th>رقم الهاتف</th>
                                                    <th>الإجراء</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($users as $user)
                                                    <tr>
                                                        <td> {{ $loop->iteration }}</td>
                                                        @if($user->img)
                                                            <td style="max-width: 100px !important; max-height: 100px !important">
                                                                <img   style="width: 50px;height: 50px;" src="{{$user->img->localUrl}}"
                                                                       class="rounded-circle">
                                                            </td>
                                                        @else
                                                            <td style="max-width: 100px !important; max-height: 100px !important">
                                                                <img   style="width: 50px;height: 50px;" src="{{asset('images/avatar-2.jpg')}}"
                                                                       class="rounded-circle">
                                                            </td>
                                                        @endif
                                                        <td>{{$user->name}} </td>
                                                        <td>{{$user->email}}</td>
                                                        <td> {{ $user->phone }}</td>
                                                        <td>
{{--                                                            @if($user->active == 1)--}}
{{--                                                                <a title="حظر" href="{{ route('admin:users.unActive', $user->id) }}" style="position:relative;width: 50px" class="btn btn-lg declinBtn btn btn-info">--}}
{{--                                                                    <i class="fa fa-ban" style="color:white;position:absolute;line-height: 24px;top:50%; margin-top: -12px;left: 7px;"></i></a>--}}
{{--                                                            @else--}}
{{--                                                                <a title="الغاء الحظر" href="{{ route('admin:users.active', $user->id) }}" style="position:relative;width: 50px" class="btn btn-lg declinBtn btn btn-success">--}}
{{--                                                                    <i class="fa fa-unlock-alt" style="color:white;position:absolute;line-height: 24px;top:50%; margin-top: -12px;left: 7px;"></i></a>--}}
{{--                                                            @endif--}}
                                                            <a onclick="redirect($(this).attr('href'));"
                                                               class="btn btn-lg views-b"
                                                               href="{{ route('admin:users.show', $user->id) }}"
                                                               title="عرض" style="background-color:green;">
                                                                <i class="fas fa-eye" style="color:white;"></i>
                                                            </a>
                                                            <a onclick="redirect($(this).attr('href'));"
                                                               href="{{ route('admin:users.edit', $user->id) }}"
                                                               class="btn btn-lg"
                                                               title="تعديل" style="position:relative;background-color:#1b6d85;width: 50px">
                                                                <i class="fas fa-wrench" style="color:white;position:absolute;line-height: 24px;top:50%; margin-top: -12px;left: 7px;"></i>
                                                            </a>

                                                            <a data-toggle="modal" data-target="#deleteModal{{$user->id}}"  title="حذف"
                                                               class="btn btn-danger btn-lg views-b views-b3" href="#" style="position:relative;">
                                                                <i class="fa fa-trash" style="color: white"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="pagination justify-content-center" style="text-align: center">
                                        {{$users->appends(request()->except('page'))->links()}}
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- [ Main Content ] end -->

                        @foreach ($users as $user)
                            @include('admin.partial.delete-modal',
                            ['id'=>$user->id,'name'=>$user->name,'route'=>route('admin:users.destroy',$user->id)])
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- datatable Js -->
{{--    <script src="{{asset('backend/assets/plugins/data-tables/js/datatables.min.js')}}"></script>--}}
{{--    <script src="{{asset('backend/assets/js/pages/tbl-datatable-custom.js')}}"></script>--}}
@endsection


