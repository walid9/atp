@extends('admin/layout')


@section('title')
    عرض بيانات العملاء
@endsection
@section('content')
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">العملاء</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href=""><i
                                                    class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ route('admin:users.index') }}">العملاء</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">عرض</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                        {{-- <div class="col-sm-12">
                                <div class="alert alert-primary" role="alert">
                                    <p>Use our extra helper file for quick setup Form Components in your page - <a href="index-form-package.html" target="_blank" class="alert-link">CHECKOUT</a></p>
                                    <label class="text-muted">Copy/paste source code in your page in just couples of seconds.</label>
                                </div>
                            </div> --}}
                        <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>عرض بيانات العميل</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="row">


                                                    <div class="col-sm-12">
                                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                            <li class="nav-item">
                                                                <a class="nav-link active text-uppercase"
                                                                   id="home-tab" data-toggle="tab" href="#home"
                                                                   role="tab" aria-controls="home"
                                                                   aria-selected="true">البيانات الشخصيه</a>
                                                            </li>


                                                            <li class="nav-item">
                                                                <a class="nav-link text-uppercase" id="contact-tab"
                                                                   data-toggle="tab" href="#contact" role="tab"
                                                                   aria-controls="contact" aria-selected="false">
                                                                    بيانات العنوان </a>
                                                            </li>

                                                        </ul>
                                                        <div style="padding: 5px 0px;" class="tab-content" id="myTab">
                                                            <div class="tab-pane fade show active" id="home"
                                                                 role="tabpanel" aria-labelledby="home-tab">
                                                                <div class="pcoded-wrapper">
                                                                    <div class="">
                                                                        <div class="pcoded-inner-content">
                                                                            <!-- [ breadcrumb ] start -->

                                                                            <!-- [ breadcrumb ] end -->
                                                                            <div class="main-body">
                                                                                <div class="page-wrapper">
                                                                                    <!-- [ Main Content ] start -->
                                                                                    <div class="row">

                                                                                        <div class="col-sm-12">
                                                                                            <div class="card">
                                                                                                <div class="card-header">
                                                                                                    <h5>المستخدم</h5>
                                                                                                </div>
                                                                                                <div class="card-body">
                                                                                                    <div class="row">

                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label for="exampleInputEmail1">الاسم
                                                                                                                    : </label>
                                                                                                                <span style="margin-right: 10px;font-weight: bold;"> {{ $user->name }}  </span>
                                                                                                            </div>
                                                                                                        </div>



                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label for="exampleInputEmail1">
                                                                                                                    البريد
                                                                                                                    الالكترونى
                                                                                                                    : </label>
                                                                                                                <span style="margin-right: 10px;font-weight: bold;"> {{ $user->email }}  </span>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label for="exampleInputEmail1">
                                                                                                                    رقم
                                                                                                                    الهاتف
                                                                                                                    :</label>
                                                                                                                <span style="margin-right: 10px;font-weight: bold;"> {{ $user->phone }} </span>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label for="exampleInputEmail1">
                                                                                                                    مفعل: </label>
                                                                                                                <span style="margin-right: 10px;font-weight: bold;"> {{ $user->active == 1 && $user->email_verified_at ? 'نعم' : 'لا'}}  </span>
                                                                                                            </div>
                                                                                                        </div>


                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label for="exampleInputEmail1">
                                                                                                                    تاريخ
                                                                                                                    الانشاء
                                                                                                                    : </label>
                                                                                                                <span style="margin-right: 10px;font-weight: bold;"> {{$user->created_at->format('Y-m-d')}}  </span>
                                                                                                            </div>
                                                                                                        </div>


                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                                <!-- [ form-element ] end -->
                                                                            </div>
                                                                            <!-- [ Main Content ] end -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="tab-pane fade" id="contact" role="tabpanel"
                                                                 aria-labelledby="contact-tab">
                                                                <div class="pcoded-wrapper">
                                                                    <div class="pcoded-content1">
                                                                        <div class="pcoded-inner-content">

                                                                            <div class="main-body">
                                                                                <div class="page-wrapper">
                                                                                    <!-- [ Main Content ] start -->
                                                                                    <div class="row">
                                                                                        <div class="col-xl-12">
                                                                                            <div class="card">
                                                                                                <div class="card-header">
                                                                                                    <h5> عرض بيانات
                                                                                                        العناوين</h5>
                                                                                                </div>
                                                                                                <div class="card-block table-border-style">
                                                                                                    <div class="table-responsive">
                                                                                                        <table id="services-table"
                                                                                                               class="display table nowrap table-striped table-hover"
                                                                                                               style="width:100%">
                                                                                                            <thead>
                                                                                                            <tr>
                                                                                                                <th>#
                                                                                                                </th>
                                                                                                                <th>
                                                                                                                    الإسم
                                                                                                                </th>
                                                                                                                <th>
                                                                                                                    العنوان
                                                                                                                </th>
                                                                                                                <th>
                                                                                                                    رقم الهاتف
                                                                                                                </th>
                                                                                                            </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                            @foreach ($user->addresses as $row)
                                                                                                                <tr>
                                                                                                                    <td> {{ $loop->iteration }}</td>
                                                                                                                    <td>{{$row->name}}</td>
                                                                                                                    <td>{{$row->address}}</td>
                                                                                                                    <td>{{$row->phone}}</td>

                                                                                                                </tr>
                                                                                                            @endforeach
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>


                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                                <!-- [ form-element ] end -->
                                                                            </div>
                                                                            <!-- [ Main Content ] end -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


@endsection
