<input type="hidden" value="{{$index}}" name="div_count" id="div-count">
<div class="row address-{{$index}}">
    <div class="col-md-2" style="display: none" id="name_{{$index}}">
        <div class="form-group">
            <label for="exampleInputEmail1">الاسم :</label>
            <input type="text" name="name_{{$index}}"
                   class="form-control
                            {{ $errors->has('name_'.$index) ? 'is-invalid' : '' }}" value="0">

            @if ($errors->has('name_'.$index))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('name_'.$index) }}</strong>
                </div>
            @endif
        </div>
    </div>

    <div class="col-md-2" style="display: none" id="address_{{$index}}">
        <div class="form-group">
            <label for="exampleInputEmail1">العنوان :</label>
            <input type="text" name="address_{{$index}}"
                   class="form-control
                            {{ $errors->has('address_'.$index) ? 'is-invalid' : '' }}" value="0">

            @if ($errors->has('address_'.$index))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('address_'.$index) }}</strong>
                </div>
            @endif
        </div>
    </div>

    <div class="col-md-2" style="display: none" id="phone_{{$index}}">
        <div class="form-group">
            <label for="exampleInputEmail1">الهاتف :</label>
            <span class="asterisk"> * </span>
            <input type="number" name="phone_{{$index}}"
                   class="form-control
                            {{ $errors->has('phone_'.$index) ? 'is-invalid' : '' }}" min="0">

            @if ($errors->has('phone_'.$index))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('phone_'.$index) }}</strong>
                </div>
            @endif
        </div>
    </div>

    <div class="form-group col-2" style="display: none" id="lat_{{$index}}">
        <label for="exampleInputEmail1">خط العرض (Latitude)</label>

        <span class="asterisk" style="color: red;"> * </span>

        <!-- Button trigger modal -->
        <a href="#" data-toggle="modal"
           data-target=".bd-example-modal-lg">
            عرض الخريطة
        </a>


        <input type="text" name="lat_{{$index}}" required id="lat{{$index}}"
               class="form-control form-control{{ $errors->has('lat_0') ? 'is-invalid' : '' }}"
        >

        @if ($errors->has('lat_'.$index))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('lat_'.$index) }}</strong>
            </div>
        @endif
    </div>


    <div class="form-group col-2" style="display: none" id="long_{{$index}}">
        <label for="exampleInputEmail1">خط الطول (Longitude)</label>
        <span class="asterisk" style="color: red;"> * </span>
        <input type="text" name="long_{{$index}}" required id="lng{{$index}}"

               class="form-control form-control{{ $errors->has('long_'.$index) ? 'is-invalid' : '' }}"
        >

        @if ($errors->has('long_'.$index))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('long_'.$index) }}</strong>
            </div>
        @endif
    </div>

</div>
</div>

    <div class="col-md-2">
        <div class="form-group">
            <button class="btn btn-sm btn-danger" type="button"
                    onclick="deleteDivAddress({{$index}})"
                    id="delete-div-{{$index}}" style="margin-top: 40px;">
                <li class="fas fa-trash"></li>
            </button>
        </div>
    </div>

</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">اختر الموقع</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" id="map">

            </div>

            <div class="modal-footer" style="direction: ltr;">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">غلق</button>
                {{--                <button type="button" class="btn btn-primary">Send message</button>--}}
            </div>

        </div>
    </div>
</div>

<script defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSvNhmIoBm2AeDEnk0QSTAutALNLu-I7Y&callback=initMap">
</script>

<script type="text/javascript">

    var index = {!! json_encode($index) !!};

    //Set up some of our variables.
    var map; //Will contain map object.
    var marker = false; ////Has the user plotted their location marker?

    //Function called to initialize / create the map.
    //This is called when the page has loaded.
    function initMap() {

        //The center location of our map.
        var centerOfMap = new google.maps.LatLng(24.68731563631883, 46.719044971885445);


        //Map options.
        var options = {
            center: centerOfMap, //Set center.
            zoom: 7 //The zoom value.
        };

        //Create the map object.
        map = new google.maps.Map(document.getElementById('map'), options);

        //Listen for any clicks on the map.
        google.maps.event.addListener(map, 'click', function (event) {
            //Get the location that the user clicked.
            var clickedLocation = event.latLng;
            //If the marker hasn't been added.
            if (marker === false) {
                //Create the marker.
                marker = new google.maps.Marker({
                    position: clickedLocation,
                    map: map,
                    draggable: true //make it draggable
                });
                //Listen for drag events!
                google.maps.event.addListener(marker, 'dragend', function (event) {
                    markerLocation();
                });
            } else {
                //Marker has already been added, so just change its location.
                marker.setPosition(clickedLocation);
            }
            //Get the marker's location.
            markerLocation();
        });
    }

    //This function will get the marker's current location and then add the lat/long
    //values to our textfields so that we can save the location.
    function markerLocation() {
        //Get location.
        var currentLocation = marker.getPosition();
        //Add lat and lng values to a field that we can save.
        document.getElementById('lat'+index).value = currentLocation.lat(); //latitude
        document.getElementById('lng'+index).value = currentLocation.lng(); //longitude
    }


    //Load the map when the page has finished loading.
    google.maps.event.addDomListener(window, 'load', initMap);

</script>
