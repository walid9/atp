@extends('admin/layout')

@section('title')
    إنشاء
@endsection
@section('content')
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">العملاء</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href=""><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin:users.index') }}">العملاء</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">@yield('title')</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                        {{-- <div class="col-sm-12">
                                <div class="alert alert-primary" role="alert">
                                    <p>Use our extra helper file for quick setup Form Components in your page - <a href="index-form-package.html" target="_blank" class="alert-link">CHECKOUT</a></p>
                                    <label class="text-muted">Copy/paste source code in your page in just couples of seconds.</label>
                                </div>
                            </div> --}}
                        <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>إنشاء جديد</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <form id="create_form" method="post" enctype="multipart/form-data" action="{{ route('admin:users.store') }}">
                                                @csrf

                                                    @include('admin.users.form')

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Input group -->
                            </div>
                            <!-- [ form-element ] end -->
                        </div>
                        <!-- [ Main Content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.getElementById("licId").style.display='none';
        var i = 0;
        document.getElementById('customFile').addEventListener('change', () => {
            document.getElementById("licId").style.display='block';
            if (i == 0) {
                i = 1;
                var elem = document.getElementById("licId");
                var width = 1;
                var id = setInterval(frame, 10);
                function frame() {
                    if (width >= 100) {
                        clearInterval(id);
                        i = 0;
                    } else {
                        width++;
                        elem.style.width = width + "%";
                        document.getElementById("labelLic").innerHTML = width * 1 + '%';
                    }
                }

            }
        });
    </script>
@endsection


@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script>

        function addAddress() {

            var div_count = $("#div-count").val();

            $.ajax({
                url: "{{route('admin:new.address.form')}}",
                type: "post",
                data:{div_count:div_count},
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $("#div-count").val(parseInt(parseInt(div_count) + parseInt(1)));
                    $(".address-form").append(data);
                    $('#name_'+$("#div-count").val()).show();
                    $('#phone_'+$("#div-count").val()).show();
                    $('#address_'+$("#div-count").val()).show();
                    $('#lat_'+$("#div-count").val()).show();
                    $('#long_'+$("#div-count").val()).show();
                }
            });
        }

        function deleteDivAddress(index) {
            $(".address-"+index).remove();
            $("#delete-div-"+index).hide();
        }


    </script>

@endsection




