<div class="row">

    <div class="form-group col-4">
        <label for="exampleInputEmail1">اسم المستخدم</label>
        <span class="asterisk" style="color: red;"> * </span>
        <input type="text" name="name" required
               value="{{old('name', isset($user)? $user->name:'')}}" class="form-control"
               placeholder="">
    </div>

    <div class="form-group col-4">
        <label for="exampleInputEmail1">البريد الإلكترونى</label>
        <span class="asterisk" style="color: red;"> * </span>
        <input type="email" name="email" value="{{old('email', isset($user)? $user->email:'')}}"
               class="form-control" placeholder="">
    </div>

    <div class="form-group col-4">
        <label for="exampleInputEmail1">كلمه المرور</label>
        @if(! isset($user))
            <span class="asterisk" style="color: red;"> * </span>
        @endif
        <input name="password" type="password"
               class="form-control m-input" placeholder="" value=""
                {{isset($user)? '':'required'}}>
    </div>

{{--    <div class="col-md-4">--}}
{{--        <div class="form-group">--}}
{{--            <label for="exampleInputEmail1">كود الدولة :</label>--}}
{{--            <span class="asterisk"> * </span>--}}
{{--            <select name="code_id" class="form-control js-example-basic-single--}}
{{--                                    {{ $errors->has('code_id') ? 'is-invalid' : '' }}" required>--}}
{{--                <option value="#"> اختر الكود</option>--}}
{{--                @foreach($codes as $code)--}}
{{--                    <option {{isset($user) && $user->code_id == $code->id ? 'selected':''}} value="{{$code->id}}">{{$code->code}}</option>--}}
{{--                @endforeach--}}
{{--            </select>--}}

{{--            @if ($errors->has('code_id'))--}}
{{--                <div class="invalid-feedback">--}}
{{--                    <strong>{{ $errors->first('code_id') }}</strong>--}}
{{--                </div>--}}
{{--            @endif--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="form-group col-4">
        <label for="exampleInputEmail1">رقم الهاتف</label>
        <span class="asterisk" style="color: red;"> * </span>
        <input name="phone" type="text" class="form-control m-input"
               value=" {{ old('phone', isset($user)? $user->phone : '') }}" required>
    </div>

    <div class="form-group col-4">
        <label for="exampleInputEmail1">الصورة</label>
        <div class="uploadt">
            <div class="uploadss"><i class="fas fa-cloud-upload-alt"></i></div>
            <input name="image" type="file" class="form-control m-input"  id="customFile" >
        </div>
        <div id="licId" style="width: 1%;height: 30px; background-color: #4CAF50;">
            <div id="labelLic" style="text-align: center;line-height: 30px; color: white;">0%</div>
        </div>
    </div>

    <div class="form-group col-2 m-t-2">
        <div class="form-group d-inline">
            <label for="exampleInputEmail1">الحالة</label>
            <br>
            <div class="checkbox checkbox-primary checkbox-fill d-inline ">

                <input type="checkbox" name="active" value="1" id="checkbox-p-infill-1"
                       {{!isset($user) ? 'checked':''}}
                        {{isset($user) && $user->active == 1 ? 'checked':''}}
                >
                <label for="checkbox-p-infill-1" class="cr">نشط</label>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row product-attributes">
            {{--  attributes fields --}}
        </div>
    </div>

    @if(isset($user))
        @include('admin.users.edit_address')
    @else
    @include('admin.users.form_address')
    @endif
</div>

<button type="submit" class="btn btn-primary">تأكيد</button>

<a href="{{ route('admin:users.index') }}">
    <button type="button" class="btn btn-danger">الغاء</button>
</a>

