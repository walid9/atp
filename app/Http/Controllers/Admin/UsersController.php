<?php

namespace App\Http\Controllers\Admin;

use App\Exports\MembersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\CreateUserRequest;
use App\Http\Requests\Admin\Users\UpdateUserRequest;
use App\Models\ShippingDetail;
use App\Models\User;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{

    public function index(Request $request)
    {
        try{

            $users = User::query();

            $show = 10;
            $sort_type = 'ASC';

            if($request->has('show') && $request['show'] != null && in_array($request['show'], [10,25,50,100])){
                $show = $request['show'];
            }

            if($request->has('search') && $request['search'] != null ){

                $users->where(function ($q) use($request){

                    $q->orWhere('name','like','%'.$request['search'].'%')
                        ->orWhere('email','like','%'.$request['search'].'%')
                        ->orWhere('phone','like','%'.$request['search'].'%');
                });
            }

            if($request->has('date_from') && $request['date_from'] != null){
                $users->whereDate('created_at','>=', $request['date_from']);
            }

            if($request->has('date_to') && $request['date_to'] != null){
                $users->whereDate('created_at','<=', $request['date_to']);
            }

            if($request->has('sort_type') && $request['sort_type'] != null){

                $sort_type = $request['sort_type'];
            }

            if($request->has('sort') && $request['sort'] != null && in_array($request['sort_type'], ['DESC','ASC'])){

                $users->orderBy($request['sort'], $sort_type);
            }

            $users = $users->where('email','!=', null)->orderBy('id' , 'desc')->paginate($show);

            return view('admin.users.index',compact('users'));

        }catch (\Exception $e){

            return redirect()->back()->with(['alert-message'=>'sorry please try later ', 'alert-type'=>'error']);
        }

    }

    public function create()
    {

        return view('admin.users.create');
    }

    public function store(CreateUserRequest $request)
    {
        try{

            $data = $request->only('name','email','phone');

            $data['active'] = 0;

            if($request->has('active')){
                $data['active'] = 1;
                $data['email_verified_at'] = Carbon::now();
            }

            $data['password'] = Hash::make($request['password']);

            $data['email_verified_at'] = Carbon::now();

            $user = User::create($data);

            if($request->file('image'))
            {
                $file=$request->file('image');
                $user->addMedia($file)->toMediaCollection('user');
            }

            $this->saveAddresses($user, $request);

            $email = User::find(1)->email;

            $admin_email = "mwalid407@gmail.com";

            @Mail::send([], [], function ($message) use ($email, $admin_email, $user) {
                $message->to($email)
                    ->subject('ATP')
                    ->setBody("A new user created with id : " . $user->id." and name : ".$user->name);
                $message->from($admin_email);
            });

        }catch (\Exception $e){

            return redirect()->back()
                ->with(['alert-message'=>'عفوا من فضلك حاول لاحقا','alert-type'=>'error']);
        }


        return redirect(route('admin:users.index'))
            ->with(['alert-message'=>' تم بنجاح ','alert-type'=>'success']);
    }

    public function show(User $user)
    {
        return view('admin.users.show',compact('user'));
    }

    public function edit(User $user)
    {
        return view('admin.users.edit',compact('user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {

        try{

            $data = $request->only('name','email',
                'phone');

            $data['active'] = 0;
            $data['email_verified_at'] = null;

            if($request->has('active')){
                $data['active'] = 1;
                $data['email_verified_at'] = Carbon::now();
            }

            if($request->has('password') && $request['password'] != null){
                $data['password'] = Hash::make($request['password']);
            }

            $user->update($data);

            if($request->file('image'))
            {
                $file=$request->file('image');
                $user->addMedia($file)->toMediaCollection('user');
            }

        }catch (\Exception $e){

            return redirect()->back()
                ->with(['alert-message'=>'عفوا من فضلك حاول لاحقا ','alert-type'=>'error']);

        }

        return redirect(route('admin:users.index'))
            ->with(['alert-message'=>' تم بنجاح ','alert-type'=>'success']);
    }

    public function destroy(User $user){

        try{

            $user->delete();

        }catch (\Exception $e){

            return redirect()->back()->with(['alert-message'=>'عفوا من فضلك حاول لاحقا','alert-type'=>'error']);
        }


        return redirect()->back()->with(['alert-message'=>'تم بنجاح','alert-type'=>'success']);
    }

    public function active(User $user)
    {
        $user->fill([
            'active' => 1
        ])->update();

        return redirect()->back()->with(['alert-message'=>'تم بنجاح','alert-type'=>'success']);
    }

    public function unActive(User $user)
    {
        $user->fill([
            'active' => 0
        ])->update();

        return redirect()->back()->with(['alert-message'=>'تم بنجاح','alert-type'=>'success']);
    }

    public function newAddressForm(Request $request)
    {
        $index = $request['div_count'] + 1;
        $tax = $request['div_count'] + 8;
        return view('admin.users.new_address', compact('index'  , 'tax'));
    }

    public function saveAddresses($user, $request)
    {
        $row_number = $request['div_count'];

        for ($i = 0; $i <= $row_number; $i++) {

            if ($request->has('name_' . $i) && $request->has('phone_' . $i)) {

                $row = $user->addresses()->create([
                    'name' => $request['name_' . $i],
                    'address' => $request['address_' . $i],
                    'phone' => $request['phone_' . $i],
                    'lat' => $request['lat_' . $i],
                    'long' => $request['long_' . $i],
                ]);
            }
        }
    }

    public function addAddress(User $user , Request $request)
    {
        $row_number = $request['div_count'];

        for ($i = 1; $i <= $row_number; $i++) {
            if ($request->has('name_' . $i) && $request->has('phone_' . $i)) {

                $row = $user->addresses()->create([
                    'name' => $request['name_' . $i],
                    'address' => $request['address_' . $i],
                    'phone' => $request['phone_' . $i],
                    'lat' => $request['lat_' . $i],
                    'long' => $request['long_' . $i],
                ]);
            }
        }

        return redirect()->back()->with(['alert-message' =>'تم اضافة العنوان بنجاح','alert-type' => 'success']);

    }

    public function ajaxUpdateAddress(Request $request)
    {
        $validationRules = [
            'name' => 'nullable|string',
            'address' => 'required',
            'index' => 'required|numeric|integer',
            'phone'=>'required',
            'lat'=>'required',
            'long'=>'required',
        ];

        $validator = Validator::make($request->all(),$validationRules);

        if ($validator->fails())
            return Response::json([$validator->errors()->first()], 400);

        $address = ShippingDetail::findOrFail($request['index']);


        $data = $request->only('name','phone', 'address' , 'lat');

        $address->update($data);


        return $data = ['success' => 'تم تعديل العنوان بنجاح'];
    }

    public function ajaxDeleteAddress(Request $request)
    {
        $price = ShippingDetail::findOrFail($request['index']);
        $price->delete();
        return $data = ['success' => 'تم حذف العنوان بنجاح'];
    }
}
