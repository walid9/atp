<?php

namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->user->id;

        if(request()->has('password') && request()->password != null)
            $rules['password'] = 'required|string|min:5';

        if(request()->has('roles')){
            $rules['roles'] = 'nullable';
            $rules['roles.*'] = 'exists:roles,id';
        }

        $rules = [
            'email' => 'required|email|unique:users,email,'.$id.',id,deleted_at,NULL' ,
            'phone' => 'required|unique:users,phone,'.$id.',id,deleted_at,NULL',
            'name' => 'required|unique:users,name,'.$id.',id,deleted_at,NULL',
            'image'=>'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        return $rules;
    }
}
