<?php

namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'=>'required|string|max:255|unique:users,name,NULL,id,deleted_at,NULL|max:255',
            'email'=>'required|email|unique:users,email,NULL,id,deleted_at,NULL|max:255',
            'password'=>'required|string|min:5',
            'image'=>'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'phone'=>'required|string|unique:users,phone,NULL,id,deleted_at,NULL',
        ];

        if(request()->has('roles')){
            $rules['roles'] = 'nullable';
            $rules['roles.*'] = 'exists:roles,id';
        }

        return $rules;
    }
}
