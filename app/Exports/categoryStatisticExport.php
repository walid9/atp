<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class categoryStatisticExport implements FromView,ShouldAutoSize,WithEvents
{
    public $report;

    public function __construct($report , $from , $to , $count)
    {
        $this->report = $report;
        $this->from = $from;
        $this->to = $to;
        $this->count =  $count;
    }

    public function view(): View
    {
        return view('admin.categories', [
            'report' => $this->report ,'action'=>'download' , 'from' => $this->from ,
            'to' => $this->to , ' count' => $this->count
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
