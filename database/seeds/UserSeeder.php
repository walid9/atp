<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $checkAdmin= User::where('email','admin@admin.com')->first();

        if(empty($checkAdmin))
            $user = User::create(
                [
                    'name' => 'admin',
                    'email'=>'admin@admin.com',
                    'password'=>bcrypt('admin')
                    ,'phone'=>'01555307734',
                    'active'=> 1,
                    'is_admin'=> 1,
                    'type'=> 'superAdmin',
                    'email_verified_at'=> \Carbon\Carbon::now(),
                ]);

    }
}
